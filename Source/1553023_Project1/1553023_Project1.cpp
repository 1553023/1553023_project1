// Win32Project2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1553023_Project1.h"

#include <CommCtrl.h>
#include<commdlg.h>
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK	WndProcBITMAP(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
ATOM MyRegisterClassBITMAP(HINSTANCE hInstance);

void CreateColorBox(HWND);
void CreateToolbar(HWND hWnd);
void doToolBar_NotifyHandle(LPARAM	lParam);
HWND hToolBarWnd ;
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32
// TRONG NHAN **********************************8

#define IDM_FIRSTCHILD 50000

int sub=0;
typedef struct tagCHILD_WND_DATA 
{
	BYTE  wndType;  
	HWND  hWnd;  
	HWND  hEditWnd; 
	BITMAP  hBitmap;  
	COLORREF rgbColor; 
	LOGFONT  logFont;   
	BYTE  drawMode;  
}
CHILD_WND_DATA;


CHILD_WND_DATA *pBITMAP;


int bitmap=0;
HMENU hMenuInit, hMenuTEXT, hMenuBITMAP;
HMENU hMenuInitWin, hMenuTEXTWin, hMenuBITMAPWin;
HMENU hMenu;

HWND hWndFrame;

BOOL CALLBACK CloseProc(HWND hWnd, LPARAM lParam);
//TRONG NHAN ************************************

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;
	
	hInst=hInstance;

	// Initialize global strings
	
	

	

	
	MyRegisterClass(hInstance);
	MyRegisterClassBITMAP(hInstance);
	
	 InitInstance(hInstance,  nCmdShow);

	hMenuInit=LoadMenu(hInstance,szWindowClass);

	hMenuInit=LoadMenu(hInstance,L"BITMAP");




	// Perform application initialization:
	


	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1553023_PROJECT1));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(hWndFrame, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	DestroyMenu(hMenuTEXT);
	DestroyMenu(hMenuBITMAP);

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1553023_PROJECT1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1553023_PROJECT1);
	wcex.lpszClassName	= L"MYPAINT";
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	
	return RegisterClassEx(&wcex);
}

ATOM MyRegisterClassBITMAP(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProcBITMAP;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= sizeof(HANDLE);
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1553023_PROJECT1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1553023_PROJECT1);
	wcex.lpszClassName	= L"BITMAP";
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWndClient;

  	hWndFrame= CreateWindow(L"MYPAINT", L"MYPAINT", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, hMenuInit, hInstance, NULL);


	hWndClient=GetWindow(hWndFrame,GW_CHILD);
	ShowWindow(hWndFrame,nCmdShow);
	UpdateWindow(hWndFrame);

   ShowWindow(hWndFrame, nCmdShow);
   UpdateWindow(hWndFrame);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static HWND hWndClient;
	CLIENTCREATESTRUCT clientcreate;
	HWND hWndChild;
	MDICREATESTRUCT mdicreate;


	switch (message)
	{
	case WM_CREATE:
		{
			
			clientcreate.hWindowMenu=GetSubMenu(GetMenu(hWnd),2);
			clientcreate.idFirstChild=IDM_FIRSTCHILD;
			hWndClient=CreateWindow(TEXT("MDICLIENT"),NULL,WS_CHILD|WS_CLIPCHILDREN|WS_VISIBLE,
				0,0,0,0, hWnd,(HMENU) 1 , hInst, (PSTR) &clientcreate);

			hMenu=GetMenu(hWnd);

			CreateToolbar(hWnd);
			return 0;

		}
	case WM_SIZE:
		{
			MoveWindow(hWndClient,0,26,LOWORD(lParam),HIWORD(lParam),TRUE);
			return 0;
		}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_WINDOW_CASCADE:
			{
				SendMessage(hWndClient,WM_MDICASCADE,0,0);
				return 0;
			}
		case ID_WINDOW_TIDE:
			{
				SendMessage(hWndClient,WM_MDITILE,0,0);
				return 0;
			}
		case ID_WINDOW_CLOSEALL:
			{
				EnumChildWindows(hWndClient,CloseProc,0);
				bitmap=0;
				
				return 0;
			}
		case ID_DRAW_RECTANGLE:
			{

			break;
				
			}
	

		
		case ID_DRAW_LINE:
			{
			
			break;
				
			}
		case ID_DRAW_ELLIPSE:
			{
				
			

			break;
			}
		case ID_DRAW_COLOR:
			{
				CreateColorBox(hWnd);
				break;
			}
		case ID_FILE_OPEN:
			{
				MessageBox(hWnd,L"Ban da chon Open",L"Notice",MB_OK);
				break;
			}
		case ID_FILE_SAVE:
			{
				MessageBox(hWnd,L"Ban da chon Save",L"Notice",MB_OK);
				break;

			}
		case ID_FILE_NEW:
			{

				bitmap++;
				TCHAR buff[250];
				wsprintf(buff,L"Nonname-%d.drw",bitmap);
				mdicreate.szClass=L"BITMAP";
				mdicreate.szTitle=buff;
				mdicreate.hOwner=hInst;
				mdicreate.x=CW_USEDEFAULT;
				mdicreate.y=CW_USEDEFAULT;
				mdicreate.cx=CW_USEDEFAULT;
				mdicreate.cy=CW_USEDEFAULT;
				mdicreate.style=0;
				mdicreate,lParam=0;
				hWndChild=(HWND) SendMessage(hWndClient,WM_MDICREATE,0,(LPARAM) (LPMDICREATESTRUCT) &mdicreate);
				return 0;
			}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefFrameProc(hWnd,hWndClient,message,wParam,lParam);

		}
		break;

		case WM_NOTIFY:
			{
		doToolBar_NotifyHandle(lParam);
			}
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefFrameProc(hWnd,hWndClient,message,wParam,lParam);
	}
	return 0;
}


LRESULT CALLBACK WndProcBITMAP(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		{
			bitmap--;
		}
		break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
BOOL CALLBACK CloseProc(HWND hWnd, LPARAM lParam)
{
	if(GetWindow(hWnd, GW_OWNER) ) return TRUE;
	SendMessage(GetParent(hWnd), WM_MDIRESTORE,(WPARAM) hWnd,0);
	if(!SendMessage(hWnd,WM_QUERYENDSESSION,0,0))
		return TRUE;
	SendMessage(GetParent(hWnd),WM_MDIDESTROY,(WPARAM) hWnd,0);
	return TRUE;
}

void CreateColorBox(HWND hWnd)
{
				CHOOSECOLOR ColorBox;                 
				static COLORREF acrCustClr[16];                    
				HBRUSH hbrush;                 
				static DWORD rgbCurrent;        

												
				SecureZeroMemory(&ColorBox, sizeof(ColorBox));
				ColorBox.lStructSize = sizeof(ColorBox);
				ColorBox.hwndOwner = hWnd;
				ColorBox.lpCustColors = (LPDWORD)acrCustClr;
				ColorBox.rgbResult = rgbCurrent;
				ColorBox.Flags = CC_NONE | CC_RGBINIT;

				if (ChooseColor(&ColorBox) == TRUE)
				{
					hbrush = CreateSolidBrush(ColorBox.rgbResult);
					rgbCurrent = ColorBox.rgbResult;
				}
				
}
void CreateToolbar(HWND hWnd)
{
		// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		
	};

	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));


	// define new buttons
	TBBUTTON tbButtons11[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons11[1].iBitmap += idx;
	tbButtons11[2].iBitmap += idx;
	tbButtons11[3].iBitmap += idx;
	tbButtons11[4].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons11) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons11);

}
void doToolBar_NotifyHandle(LPARAM	lParam)
{

	LPTOOLTIPTEXT   lpToolTipText;
	TCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	// ToolTipText, loaded from Stringtable resource

	// lParam: address of TOOLTIPTEXT struct
	lpToolTipText = (LPTOOLTIPTEXT)lParam;

	if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
	{
		// hdr.iFrom: ID cua ToolBar button -> ID cua ToolTipText string
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);

		lpToolTipText->lpszText = szToolTipText;
	}
}